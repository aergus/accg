module Main where

import ACCG.Game.Run (runGame)
import ACCG.Game.Initialization (initStateFor)
import ACCG.Game.StandardParameters (stdParametersFor)
import ACCG.Strategies.Encoding (encodingStrategy)
import ACCG.Strategies.Straightforward (straightforwardStrategy)
import ACCG.Types.Strategy (Strategy)
import ACCG.Utilities.Score (extendedScore, rawScore)

import Control.Monad (replicateM_)
import Data.Ratio (Ratio)

import qualified Data.Map as Map (fromList)

numPlayers :: Int
numPlayers = 4

runGameForCopies :: Strategy IO -> Int -> IO ()
runGameForCopies str n = do
  initState <- initStateFor $ stdParametersFor n
  endState <- runGame initState $
    Map.fromList $ zip [0 .. numPlayers - 1] (replicate n str)
  print (rawScore endState, extendedScore endState :: Ratio Int)

numTries :: Int
numTries = 100

main :: IO ()
main = do
  putStrLn $ "Running the encoding strategy " ++ show numTries ++ " times..."
  replicateM_ numTries $ runGameForCopies encodingStrategy numPlayers
  putStrLn $
    "Running the straightforward strategy " ++ show numTries ++ " times..."
  replicateM_ numTries $ runGameForCopies straightforwardStrategy numPlayers
