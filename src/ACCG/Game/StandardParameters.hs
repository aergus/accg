module ACCG.Game.StandardParameters (stdParametersFor, stdCardsPerPlayer) where

import ACCG.Types.Card (Card(..))
import ACCG.Types.Color (singleColors)
import ACCG.Types.GameParameters (GameParameters(..))


{- |
  Standard 'ACCG.Types.GameParameters.GameParameters' for a given number of
  players.

  > stdParametersFor n = GP
  >   { _maxHints         = 8
  >   , _maxMistakes      = 2
  >   , _numPlayers       = n
  >   , _numCards         = sum $ map (* length singleColors) cpPerNum
  >   , _colorSets        = singleColors
  >   , _cardsPerPlayer   = stdCardsPerPlayer n
  >   , _cardsPerColorSet = 5
  >   , _copiesPerNum     = cpPerNum
  >   , _extraHintFor     =
  >       [Crd {colorSet = _clrS, _number = 5} | clrS <- singleColors]
  >   }
  >   where cpPerNum = [3, 2, 2, 2, 1]
-}
stdParametersFor :: Int -> GameParameters
stdParametersFor n = GP
  { _maxHints         = 8
  , _maxMistakes      = 2
  , _numPlayers       = n
  , _numCards         = sum $ map (* length singleColors) cpPerNum
  , _colorSets        = singleColors
  , _cardsPerPlayer   = stdCardsPerPlayer n
  , _cardsPerColorSet = 5
  , _copiesPerNum     = cpPerNum
  , _extraHintFor     =
      [Crd {_colorSet = clrS, _number = 5} | clrS <- singleColors]
  }
  where cpPerNum = [3, 2, 2, 2, 1]

{- |
  Cards per player in the standard game.

  > stdCardsPerPlayer 2 = 5
  > stdCardsPerPlayer 3 = 5
  > stdCardsPerPlayer 4 = 4
  > stdCardsPerPlayer 5 = 4
  > stdCardsPerPlayer _ = 3
-}
stdCardsPerPlayer :: Int -> Int
stdCardsPerPlayer 2 = 5
stdCardsPerPlayer 3 = 5
stdCardsPerPlayer 4 = 4
stdCardsPerPlayer 5 = 4
stdCardsPerPlayer _ = 3
