module ACCG.Game.Run (runGame, gameEnded) where

import ACCG.Game.Mechanics (transactMove)
import ACCG.Types.GameParameters (maxMistakes, numCards)
import ACCG.Types.GameState (GameState, currentIndex, publicKnowledge)
import ACCG.Types.PlayerIndex (PlayerIndex)
import ACCG.Types.PlayerKnowledge (knowledgeOf)
import ACCG.Types.PublicKnowledge ( parameters
                                  , mistakes
                                  , roundsLeft
                                  )
import ACCG.Types.Strategy (Strategy, runStrategy, updateStrategy)
import ACCG.Utilities.Score (rawScore)

import Control.Lens ((^.))

import qualified Data.Map as Map (Map, (!), insert, traverseWithKey)

{- |
  Takes an inital 'ACCG.Types.GameState.GameState', calls the strategies
  'ACCG.Types.Strategy.Strategy' and applies the resulting moves one by one,
  and stops when 'ACCG.Game.Run.gameEnded' yields @True@.
-}
runGame ::
  Monad m => GameState -> Map.Map PlayerIndex (Strategy m) -> m GameState
runGame s ps = do
  ps' <- Map.traverseWithKey (\ i str -> updateStrategy str $ knowledgeOf i s)
                             ps
  runGame' s ps'

runGame' ::
  Monad m => GameState -> Map.Map PlayerIndex (Strategy m) -> m GameState
runGame' s ps | gameEnded s = return s
              | otherwise   = do
  let ind             = currentIndex s
      p               = ps Map.! ind
      playerKnowledge = knowledgeOf ind s
  (move, newP) <- runStrategy p playerKnowledge
  state <- transactMove move s
  ps' <- Map.traverseWithKey
    (\ i str -> updateStrategy str (knowledgeOf i state))
    (Map.insert ind newP ps)
  runGame' state ps'

{- |
  Checks if the game has ended, which is the case when one of the following
  happens:

    * More mistakes have been made than the maximal number of mistakes.
    * The player who drew the last card from the card pile had another turn
  after the last card was drawn.
    * The board is complete, i.e. a card of each color set-number combination
  has been playerd.
-}
gameEnded :: GameState -> Bool
gameEnded s = pubKnow ^. mistakes > params ^. maxMistakes ||
              pubKnow ^. roundsLeft == Just 0 ||
              rawScore s  == params ^. numCards
  where params   = pubKnow ^. parameters
        pubKnow  = s ^. publicKnowledge
