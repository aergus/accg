module ACCG.Game.Mechanics (cardFits, transactMove) where

import ACCG.Types.Card (Card, colorSet, number)
import ACCG.Types.GameParameters ( cardsPerColorSet
                                 , extraHintFor
                                 , maxHints
                                 , numPlayers
                                 )
import ACCG.Types.GameState ( GameState
                            , currentIndex
                            , currentPlayer
                            , pile
                            , players
                            , publicKnowledge
                            )
import ACCG.Types.Hint (Hint(ColorHint, NumberHint))
import ACCG.Types.InPile (InPile (Discarded, OnBoard))
import ACCG.Types.LogEntry (LogEntry(..))
import ACCG.Types.Move (Move(Discard, Play, Tell))
import ACCG.Types.Player (Player, cardOf, hand)
import ACCG.Types.PlayerIndex (PlayerIndex)
import ACCG.Types.PublicKnowledge ( PublicKnowledge
                                  , board
                                  , counter
                                  , discarded
                                  , gameLog
                                  , hintsLeft
                                  , mistakes
                                  , parameters
                                  , roundsLeft
                                  )

import Control.Lens ((^.), over)
import Data.Maybe (isNothing)

import qualified Data.Map.Lazy as Map ((!), adjust, keys)

{- |
  Transacts a move, applying it and doing all the book keeping (e.g.
  logging, incrementing some counters if needed) along the way.

  It 'fail's with a descriptive error message if the input is an
  illegal move.
-}
transactMove :: Monad m => Move -> GameState -> m GameState
transactMove m s = do
  s' <- applyMove m s
  return $
    ( over (publicKnowledge . roundsLeft)
           (\ rl -> if isNothing rl && null (s' ^. pile)
                    then Just (s' ^. publicKnowledge . parameters . numPlayers)
                    else fmap (\ n -> n - 1) rl)
    . over (publicKnowledge . counter) (+1)
    ) s'

-- TODO: better error handling
applyMove :: Monad m => Move -> GameState -> m GameState
applyMove m@(Play n) s | not $ hasCardAt ply n =
  fail "Trying to play a non-existent card..."
                       | cardFits pubKnow crd  =
  ( return
  . over (publicKnowledge . gameLog) (logEntryWith s m (Just $ OnBoard crd) :)
  . drawCardForCurrent
  . over (publicKnowledge . hintsLeft)
         (\ hl -> if crd `elem` params ^. extraHintFor
                  then min (hl + 1) (params ^. maxHints)
                  else hl)
  . over (publicKnowledge . board) (Map.adjust (+1) (crd ^. colorSet))
  . over players (Map.adjust (deleteCardOf n) (currentIndex s))
  ) s
                       | otherwise              =
  ( return
  . over (publicKnowledge . gameLog) (logEntryWith s m (Just $ Discarded crd) :)
  . over (publicKnowledge . mistakes) (+ 1)
  . over (publicKnowledge . discarded) (Map.adjust (+ 1) crd)
  . discardCardOfCurrent n
  ) s
  where crd     = fst $  (!! n) $ ply ^. hand
        ply     = currentPlayer s
        params  = pubKnow ^. parameters
        pubKnow = s ^. publicKnowledge
applyMove m@(Discard n) s | not $ hasCardAt ply n =
    fail "Trying to discard a non-existent card..."
                          | otherwise             =
  ( return
  . over (publicKnowledge . gameLog)
         (logEntryWith s m (Just $ Discarded $ cardOf n ply) :)
  . over (publicKnowledge . hintsLeft)
         (\ hl -> min (hl + 1) (s ^. publicKnowledge . parameters . maxHints))
  . discardCardOfCurrent n
  ) s
  where ply = currentPlayer s
applyMove m@(Tell i h)  s | pubKnow ^. hintsLeft <= 0                      =
  fail "Trying to give a hint when there are no hints left..."
                          | notElem i $ Map.keys $ s ^. players            =
  fail "Trying to give a hint to a non-existent player..."
                          | otherwise                                      =
    case h of
      ColorHint  c -> return $ mapToPlayerCards h
                                                (\ cd ->
                                                   c `elem` cd ^. colorSet)
                                                i
                                                newS
      NumberHint k -> if k < 1 ||  k > pubKnow ^. parameters . cardsPerColorSet
                      then fail ("Trying to give a hint about " ++
                                 "numbers that do not exist.")
                      else return $ mapToPlayerCards h
                                                     (\ cd -> cd ^. number == k)
                                                     i
                                                     newS
  where newS     =
          ( over (publicKnowledge . gameLog) (logEntryWith s m Nothing :)
          . over (publicKnowledge . hintsLeft) (\ n -> n - 1)
          ) s
        pubKnow  = s ^. publicKnowledge

cardFits :: PublicKnowledge -> Card -> Bool
cardFits pubKnow crd =
  crd ^. number == (pubKnow ^. board) Map.! (crd ^. colorSet) + 1

hasCardAt :: Player -> Int -> Bool
hasCardAt p n = n >= 0 && length (p ^. hand) > n

mapToPlayerCards ::
  Hint -> (Card -> Bool) -> PlayerIndex -> GameState -> GameState
mapToPlayerCards h f i = over players $
  Map.adjust (over hand (map (\ (crd, hs) -> if f crd
                                             then (crd, h:hs)
                                             else (crd, hs)
                             )
                        )
             )
             i

deleteCardOf :: Int -> Player -> Player
deleteCardOf n = over hand (\ h -> take n h ++ drop (n + 1) h)

discardCardOfCurrent :: Int -> GameState -> GameState
discardCardOfCurrent n s =
  ( drawCardForCurrent
  . over players (Map.adjust (deleteCardOf n) ind)
  . over (publicKnowledge . discarded) (Map.adjust (+ 1) (cardOf n ply))
  ) s
  where ply = currentPlayer s
        ind = currentIndex s

drawCardForCurrent :: GameState -> GameState
drawCardForCurrent s | (not . null . (^. pile)) s =
  ( over pile tail
  . over players
         (Map.adjust (over hand (++ [(head (s ^. pile), [])])) (currentIndex s))
  ) s
                     | otherwise                = s

logEntryWith :: GameState -> Move -> Maybe (InPile Card) -> LogEntry
logEntryWith s m c =  LE
  { _turn        = s ^. publicKnowledge . counter
  , _playerIndex = currentIndex s
  , _move        = m
  , _card        = c
  }
