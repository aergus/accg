module ACCG.Game.Initialization (initStateFor) where

import ACCG.Types.GameParameters ( GameParameters
                                 , cardsFor
                                 , cardsPerPlayer
                                 , numPlayers
                                 )
import ACCG.Types.GameState (GameState(..))
import ACCG.Types.Player (Player(..))
import ACCG.Types.PublicKnowledge (initPubKnow)

import Control.Lens ((^.))
import Control.Monad.Random (MonadRandom)
import System.Random.Shuffle (shuffleM)

import qualified Data.Map as Map (empty, insert)

{- |
   Produces an initial 'ACCG.Types.GameState.GameState' with a shuffled pile
   from given 'ACCG.Types.GameParameters.GameParameters'.
-}
initStateFor :: MonadRandom m => GameParameters-> m GameState
initStateFor p =
  do cards <- shuffleM $ cardsFor p
     let k = p ^. cardsPerPlayer
         (cds, pls) =
           foldr (\ i (cs, ps) -> ( drop k cs
                                  , Map.insert i
                                               P { _hand = map (\ c -> (c, []))
                                                               (take k cs)
                                                 }
                                               ps
                                  )
                 )
                 (cards, Map.empty)
                 [0 .. (p ^. numPlayers - 1)]
     return GS
       { _pile = cds
       , _players = pls
       , _publicKnowledge = initPubKnow p
       }
