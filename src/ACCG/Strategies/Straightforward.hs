module ACCG.Strategies.Straightforward (straightforwardStrategy) where

import ACCG.Types.Board (Board)
import ACCG.Types.Card (Card(..), colorSet, number)
import ACCG.Types.Color (Color)
import ACCG.Types.GameParameters (maxMistakes)
import ACCG.Types.Hint (Hint(ColorHint, NumberHint))
import ACCG.Types.Move (Move(Discard, Play, Tell))
import ACCG.Types.Player (Player, hand)
import ACCG.Types.PlayerKnowledge (PlayerKnowledge(PlyK))
import ACCG.Types.PublicKnowledge ( board
                                  , hintsLeft
                                  , mistakes
                                  , parameters
                                  )
import ACCG.Types.Strategy (Strategy, makeStatelessStrategy)

import Control.Lens ((^.))
import Control.Monad (join)
import Data.List (elemIndex, find, findIndex)
import Data.Maybe (isJust, isNothing, fromJust)

import qualified Data.Map.Lazy as Map ((!), filter, map, member, null, toList)

{- |
  A straightforward strategy which essentially plays a card if some information
  about the card is available and (otherwise) gives a hint if some card of
  another player is playable.
-}
straightforwardStrategy :: Monad m => Strategy m
straightforwardStrategy =
  makeStatelessStrategy (return . straightforwardStrategy')

straightforwardStrategy' :: PlayerKnowledge -> Move
straightforwardStrategy' (PlyK _ oth hnd pbl) | pbl ^. hintsLeft == 0       =
  Discard discardIndex
                                              | isJust maybeCrdIndex        =
  Play crdIndex
                                              | not $ Map.null obviousHints =
  uncurry Tell $ head $ Map.toList obviousHints
                                              | otherwise                   =
  Discard discardIndex
  where discardIndex | isJust maybeNoHintIndex  =
          reverseMaybeIndex maybeNoHintIndex
                     | isJust maybeNotDoneIndex =
          reverseMaybeIndex maybeNotDoneIndex
                     | otherwise                =
          lastIndex
        maybeNotDoneIndex   = findIndex not playabilities
        maybeNoHintIndex    = findIndex null reversedHand
        crdIndex            = reverseMaybeIndex maybeCrdIndex
        maybeCrdIndex | noMistakesAllowed =
          elemIndex (Just True) (map (fmap $ isPlayable brd) assembledCards)
                      | otherwise         =
          findIndex id playabilities
        playabilities       = map (playability brd) reversedHand
        assembledCards      = map assembleCard reversedHand
        noMistakesAllowed   =
          pbl ^. mistakes == pbl ^. parameters . maxMistakes - 1
        reverseMaybeIndex x = lastIndex - fromJust x
        lastIndex           = length hnd - 1
        reversedHand        = reverse hnd
        obviousHints        = Map.map head $
          Map.filter (not . null) $ Map.map (obviousHintsFor brd) oth
        brd                 = pbl ^. board

playability :: Board -> [Hint] -> Bool
playability m hs = isJust maybeNmb &&
                   and (Map.map (\ k -> k == nmb - 1) m) ||
                   not (null $ colorsIn hs)
  where nmb      = fromJust maybeNmb
        maybeNmb = findNumberHint hs

-- This won't work well with multiple colors per card!
assembleCard :: [Hint] -> Maybe Card
assembleCard hs = do nmb <- findNumberHint hs
                     clrS <- maybeColors
                     return Crd { _colorSet = clrS, _number = nmb }
  where maybeColors    = if null occuringColors
                         then Nothing
                         else Just occuringColors
        occuringColors = colorsIn hs

obviousHintsFor :: Board -> Player -> [Hint]
obviousHintsFor b p =
  concatMap (\ (cd, hs) ->
               if isPlayable b cd
               then [ NumberHint $ cd ^. number
                    | isNothing $ findNumberHint hs
                    ] ++
                    map ColorHint
                        (filter (not . (`elem` colorsIn hs)) (cd ^. colorSet))
               else [])
           (p ^. hand)

isPlayable :: Board -> Card -> Bool
isPlayable m c = Map.member colorS m && (m Map.! colorS == c ^. number - 1)
  where colorS = c ^. colorSet

colorsIn :: [Hint] -> [Color]
colorsIn = foldr (\ h cs -> (case h of
                               ColorHint c -> c:cs
                               _           -> cs))
                 []

findNumberHint :: [Hint] -> Maybe Int
findNumberHint =
  join . find isJust . map (\ h -> (case h of
                                      NumberHint n -> Just n
                                      _            -> Nothing))
