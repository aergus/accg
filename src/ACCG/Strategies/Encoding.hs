{-# LANGUAGE TemplateHaskell #-}

module ACCG.Strategies.Encoding (encodingStrategy) where

import ACCG.Game.Mechanics (cardFits)
import ACCG.Types.Card (Card (..), colorSet, number)
import ACCG.Types.Color (Color)
import ACCG.Types.GameParameters ( cardsPerColorSet
                                 , colorSets
                                 , numPlayers
                                 )
import ACCG.Types.Hint (Hint(..))
import ACCG.Types.LogEntry (playerIndex, move)
import ACCG.Types.Move (Move(..))
import ACCG.Types.Player (hand)
import ACCG.Types.PlayerIndex (PlayerIndex)
import ACCG.Types.PlayerKnowledge ( PlayerKnowledge
                                  , handKnowledge
                                  , myIndex
                                  , others
                                  , public
                                  )
import ACCG.Types.PublicKnowledge ( PublicKnowledge
                                  , board
                                  , gameLog
                                  , hintsLeft
                                  , parameters
                                  )
import ACCG.Types.Strategy (Strategy(..))

import Control.Lens ((^.), (.=), (%=), makeLenses, use)
import Control.Monad (when)
import Control.Monad.State.Lazy (StateT, lift)
import Data.List (elemIndex, sort)
import Data.Maybe (fromJust, fromMaybe, listToMaybe)

import qualified Data.Map as Map ( Map
                                 , (!)
                                 , adjust
                                 , delete
                                 , empty
                                 , fromList
                                 , keys
                                 , mapMaybeWithKey
                                 , mapWithKey
                                 )

data EncodingState = ES
  { _myHand     :: [Card]
  , _othersKnow :: Map.Map PlayerIndex Int
  }

makeLenses ''EncodingState

encodingStrategy :: Monad m => Strategy m
encodingStrategy = S
  { strategyState  = ES [] Map.empty
  , strategyUpdate = encodingUpdate
  , strategyAction = encodingAction
  }

-- TODO: add an error/warning for the case when game parameters are unsuitable
encodingUpdate :: Monad m => PlayerKnowledge -> StateT EncodingState m ()
encodingUpdate pk | null gLog          = do
  let params = pk ^. public . parameters
      numPly = params ^. numPlayers
  myHand     .= []
  othersKnow .=
    Map.delete (pk ^. myIndex) (Map.fromList (zip [0 .. numPly - 1] (repeat 0)))
                  | i == pk ^. myIndex = lift $ return ()
                  | otherwise          =
  let m = entry ^. move
   in case m of (Play n)    -> applyDiscard i n
                (Discard n) -> applyDiscard i n
                (Tell j h)  -> do
                  othersK <- use othersKnow
                  let othersSum = cardsSum pk (Map.delete i othersK)
                      modulus   = modulusFor pk
                      cardCode' =
                        (encodeTell pk i j h - othersSum) `mod` modulus
                      cardCode  = if cardCode' >= 0
                                  then cardCode'
                                  else cardCode' + modulus
                      myCard    = decodeCard pk cardCode
                  myHand %= addNew pk myCard
                  incrementOthersExcept pk (Just i)
  where gLog  = pk ^. public . gameLog
        entry = head gLog
        i     = entry ^. playerIndex

encodingAction :: Monad m => PlayerKnowledge -> StateT EncodingState m Move
encodingAction pk = do
  mh <- use myHand
  let pubK    = pk ^. public
      handFit = map (cardFits pubK) mh
      handDis = map (cardDiscardable pubK) mh
  if or handFit
  then do let index = fromJust $ elemIndex True handFit
          discardOwnCard index
          lift $ return $ Play index
  else if pubK ^. hintsLeft == 0
       then do let index = fromMaybe 0 $ elemIndex True handDis
               discardOwnCard index
               lift $ return $ Discard index
       else do
         othersK <- use othersKnow
         let cutSum = cardsSum pk othersK `mod` modulusFor pk
         incrementOthersExcept pk Nothing
         lift $ return $ decodeTell pk cutSum

incrementOthersExcept ::
  Monad m => PlayerKnowledge -> Maybe PlayerIndex -> StateT EncodingState m ()
incrementOthersExcept pk maybeI =
  othersKnow %= Map.mapWithKey (\ k l ->
                                  if maybeI == Just k
                                  then l
                                  else min (l + 1)
                                           (length $
                                              ((pk ^. others) Map.! k) ^. hand
                                           )
                               )

discardOwnCard :: Monad m => Int -> StateT EncodingState m ()
discardOwnCard i = do
  mh <- use myHand
  myHand .= (take i mh ++ drop (i + 1) mh)

cardDiscardable :: PublicKnowledge -> Card -> Bool
cardDiscardable pubK crd =
  crd ^. number <= (pubK ^. board) Map.! (crd ^. colorSet)

applyDiscard ::
  Monad m => PlayerIndex -> Int -> StateT EncodingState m ()
applyDiscard i n = do
  othersK <- use othersKnow
  when (n < (othersK Map.! i)) (othersKnow %= Map.adjust (\ l -> l - 1) i)

encodeCard :: PlayerKnowledge -> Card -> Int
encodeCard pk (Crd i l) =
  let perCS = pk ^. public . parameters . cardsPerColorSet
   in i - 1 + perCS * encodeColors pk l

decodeCard :: PlayerKnowledge -> Int -> Card
decodeCard pk i =
  let perCS = pk ^. public . parameters . cardsPerColorSet
   in Crd { _number   = i `mod` perCS + 1
          , _colorSet = decodeColors pk (i `div` perCS)
          }

encodeColors :: PlayerKnowledge -> [Color] -> Int
encodeColors pk l =
  fromJust $ elemIndex l (pk ^. public . parameters . colorSets)

decodeColors :: PlayerKnowledge -> Int -> [Color]
decodeColors pk i = (pk ^. public . parameters . colorSets) !! i

encodeHint :: PlayerKnowledge -> Hint -> Int
encodeHint pk (ColorHint c)  =
  encodeColors pk [c] + pk ^. public . parameters . cardsPerColorSet
encodeHint _  (NumberHint n) = n - 1

decodeHint :: PlayerKnowledge -> Int -> Hint
decodeHint pk n | n < treshold = NumberHint (n + 1)
                | otherwise    =
  ColorHint $ head $ decodeColors pk (n - treshold)
  where treshold = pk ^. public . parameters . cardsPerColorSet

encodeTell :: PlayerKnowledge -> PlayerIndex -> PlayerIndex -> Hint -> Int
encodeTell pk i j h =
  let playerCode = fromJust $ elemIndex j $ sort $
        pk ^. myIndex : Map.keys (Map.delete i (pk ^. others))
      hintCode   = encodeHint pk h
   in playerCode + hintCode * (pk ^. public . parameters . numPlayers - 1)

decodeTell :: PlayerKnowledge -> Int -> Move
decodeTell pk i =
  let modulus = pk ^. public ^. parameters ^. numPlayers - 1
      index   = sort (Map.keys $ pk ^. others) !! (i `mod` modulus)
      hint    = decodeHint pk (i `div` modulus)
   in Tell index hint

cardsSum :: PlayerKnowledge -> Map.Map PlayerIndex Int -> Int
cardsSum pk otK =
  let ps = pk ^. others
   in sum $ Map.mapMaybeWithKey (\ k l -> fmap (encodeCard pk . fst) $
                                   listToMaybe $ drop l $
                                   (ps Map.! k) ^. hand
                                )
                                otK

modulusFor :: PlayerKnowledge -> Int
modulusFor pk =
  let params     = pk ^. public . parameters
      colorsLen  = length (params ^. colorSets)
      cardsPerCS = params ^. cardsPerColorSet
   in colorsLen * cardsPerCS

addNew :: PlayerKnowledge -> a -> [a] -> [a]
addNew pk x xs = if length xs >= length (pk ^. handKnowledge)
                 then xs
                 else xs ++ [x]
