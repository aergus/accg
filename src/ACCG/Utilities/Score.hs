module ACCG.Utilities.Score (rawScore, extendedScore) where

import ACCG.Types.GameParameters (maxMistakes, numCards)
import ACCG.Types.GameState (GameState, publicKnowledge)
import ACCG.Types.PublicKnowledge ( board
                                  , gameLog
                                  , mistakes
                                  , parameters
                                  )

import Control.Lens ((^.))
import Data.Ratio (Ratio, (%))

{- |
  Computes the raw score for a given 'ACCG.Types.GameState.GameState'.

  > rawScore = sum . (^. publicKnowledge . board)
-}
rawScore :: GameState -> Int
rawScore = sum . (^. publicKnowledge . board)

{- |
  Computes the so-called extented score a given
  'ACCG.Types.GameState.GameState'.

  The extended score is sum of the following:

    * raw score: computed by 'ACCG.Utilities.Score.rawScore'
    * mistake score: - current number of mistakes / (maximal possible number of
  mistakes + 1)
    * turn score: - number of turns passed / ((maximal possible number of
  mistakes + 1) * (total number of cards in the game + 1))
-}
extendedScore :: Integral a => GameState -> Ratio a
extendedScore s = rScore + mistakeScore + turnScore
  where turnScore     = - toEnum (length (pubKnow ^. gameLog)) % turnFactor
        turnFactor    = mistakeFactor *
                        toEnum (params ^. numCards + 1)
        mistakeScore  = - toEnum (pubKnow ^. mistakes) % mistakeFactor
        mistakeFactor = toEnum (params ^. maxMistakes + 1)
        rScore        = toEnum (rawScore s) % 1
        params        = pubKnow ^. parameters
        pubKnow       = s ^. publicKnowledge
