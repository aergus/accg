module ACCG.Utilities.REPL (replPlayer) where

import ACCG.Types.Move (Move)
import ACCG.Types.PlayerKnowledge (PlayerKnowledge)
import ACCG.Types.Strategy (Strategy, makeStatelessStrategy)

import Text.Show.Pretty (ppShow)

{- |
  Read-evaluate-print loop for ACCG.

  It successively prints player knowledge for each player and asks for a move
  from them. Moves are read with the derived 'Read' instance of
  'ACCG.Types.Move.Move'.
-}
replPlayer :: Strategy IO
replPlayer = makeStatelessStrategy replPlayer'

replPlayer' :: PlayerKnowledge -> IO Move
replPlayer' pk = do putStrLn $ "\n" ++  ppShow pk
                    readLn
