{-# LANGUAGE TemplateHaskell #-}

module ACCG.Types.LogEntry (LogEntry(..), card, move, playerIndex, turn) where

import ACCG.Types.Card (Card)
import ACCG.Types.CounterVal (CounterVal)
import ACCG.Types.InPile (InPile)
import ACCG.Types.Move (Move)
import ACCG.Types.PlayerIndex (PlayerIndex)

import Control.Lens (makeLenses)

data LogEntry = LE
  { _turn        :: CounterVal
  , _playerIndex :: PlayerIndex
  , _move        :: Move
  , _card        :: Maybe (InPile Card)
  } deriving (Eq, Show)

makeLenses ''LogEntry
