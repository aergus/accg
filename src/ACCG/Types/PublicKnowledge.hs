{-# LANGUAGE TemplateHaskell #-}

module ACCG.Types.PublicKnowledge ( PublicKnowledge(..)
                                  , board
                                  , counter
                                  , discarded
                                  , gameLog
                                  , hintsLeft
                                  , initPubKnow
                                  , mistakes
                                  , parameters
                                  , roundsLeft
                                  ) where

import ACCG.Types.Board (Board)
import ACCG.Types.Card (Card(..))
import ACCG.Types.CounterVal (CounterVal)
import ACCG.Types.GameParameters ( GameParameters
                                 , cardsPerColorSet
                                 , colorSets
                                 , maxHints
                                 )
import ACCG.Types.LogEntry (LogEntry)

import Control.Lens ((^.), makeLenses)

import qualified Data.Map.Lazy as Map (Map, fromList)

data PublicKnowledge = PubK
  { _discarded  :: Map.Map Card Int
  , _board      :: Board
  , _hintsLeft  :: Int
  , _mistakes   :: Int
  , _roundsLeft :: Maybe CounterVal
  , _counter    :: CounterVal
  , _gameLog    :: [LogEntry]
  , _parameters :: GameParameters
  } deriving (Eq, Show)

makeLenses ''PublicKnowledge

{- |
  Creates the initial 'ACCG.Types.PublicKnowledge.PublicKnowledge' from
  given 'ACCG.Types.GameParameters.GameParameters', setting all relevant
  counters to @0@.
-}
initPubKnow :: GameParameters -> PublicKnowledge
initPubKnow p = PubK
  { _discarded  = Map.fromList
      [(Crd {_colorSet = cs, _number = k}, 0) |
       cs <- clrS, k <- [1 .. (p ^. cardsPerColorSet) ]]
  , _board      = Map.fromList [(c, 0) | c <- clrS]
  , _hintsLeft  = p ^. maxHints
  , _mistakes   = 0
  , _roundsLeft = Nothing
  , _counter    = 0
  , _gameLog    = []
  , _parameters = p
  }
  where clrS = p ^. colorSets
