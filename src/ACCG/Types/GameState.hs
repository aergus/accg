{-# LANGUAGE TemplateHaskell #-}

module ACCG.Types.GameState ( GameState(..)
                            , currentIndex
                            , currentPlayer
                            , pile
                            , players
                            , publicKnowledge
                            ) where

import ACCG.Types.Card (Card)
import ACCG.Types.GameParameters (numPlayers)
import ACCG.Types.Player (Player)
import ACCG.Types.PlayerIndex (PlayerIndex)
import ACCG.Types.PublicKnowledge (PublicKnowledge, counter, parameters)

import Control.Lens ((^.), makeLenses)

import qualified Data.Map.Lazy as Map (Map, (!))

data GameState = GS
  { _pile :: [Card]
  , _players :: Map.Map PlayerIndex Player
  , _publicKnowledge :: PublicKnowledge
  } deriving (Eq, Show)

makeLenses ''GameState

-- | > currentPlayer s = (s ^. players) Map.! currentIndex s
currentPlayer :: GameState -> Player
currentPlayer s = (s ^. players) Map.! currentIndex s

currentIndex :: GameState -> PlayerIndex
currentIndex s =
  let pk = s ^. publicKnowledge
   in pk ^. counter `mod` pk ^. parameters . numPlayers
