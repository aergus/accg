{-# LANGUAGE TemplateHaskell #-}

module ACCG.Types.Player (Player(..), cardOf, hand) where

import ACCG.Types.Card (Card)
import ACCG.Types.Hint (Hint)

import Control.Lens ((^.), makeLenses)

data Player = P { _hand :: [(Card, [Hint])] } deriving (Eq, Show)

makeLenses ''Player

-- | > cardOf n = fst . (!! n) . (^. hand)
cardOf :: Int -> Player -> Card
cardOf n = fst . (!! n) . (^. hand)
