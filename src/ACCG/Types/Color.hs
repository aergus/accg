module ACCG.Types.Color (Color(..), singleColors) where

newtype Color = Clr Int deriving (Eq, Read, Show, Ord)

blue :: Color
blue = Clr 1

green :: Color
green = Clr 2

red :: Color
red = Clr 3

white :: Color
white = Clr 4

yellow :: Color
yellow = Clr 5

{- |
  A list consisting of singletons of 'Int's which correspond to the standard
  colors of the game (blue, green, red, white and yellow).
-}
singleColors :: [[Color]]
singleColors = map return [blue, green, red, white, yellow]
