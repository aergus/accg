{-# LANGUAGE ExistentialQuantification #-}

module ACCG.Types.Strategy ( Strategy (..)
                           , makeStatelessStrategy
                           , trivialUpdate
                           , runStrategy
                           , updateStrategy
                           ) where

import ACCG.Types.Move (Move)
import ACCG.Types.PlayerKnowledge (PlayerKnowledge)

import Control.Monad.State.Lazy (StateT, execStateT, lift, runStateT)

data Strategy m = forall s. S
  { strategyState  :: s
  , strategyUpdate :: PlayerKnowledge -> StateT s m ()
  , strategyAction :: PlayerKnowledge -> StateT s m Move
  }

trivialUpdate :: Monad m => PlayerKnowledge -> StateT s m ()
trivialUpdate = const $ lift $ return ()

updateStrategy :: Monad m => Strategy m -> PlayerKnowledge -> m (Strategy m)
updateStrategy S { strategyState  = state
                 , strategyUpdate = upd
                 , strategyAction = act
                 }
               pk
  = do newState <- execStateT (upd pk) state
       return S
         { strategyState  = newState
         , strategyUpdate = upd
         , strategyAction = act
         }

runStrategy :: Monad m => Strategy m -> PlayerKnowledge -> m (Move, Strategy m)
runStrategy S { strategyState  = state
              , strategyUpdate = upd
              , strategyAction = act
              }
            pk
  = do (move, newState) <- runStateT (act pk) state
       return ( move
              , S { strategyState  = newState
                  , strategyUpdate = upd
                  , strategyAction = act
                  }
              )

makeStatelessStrategy :: Monad m => (PlayerKnowledge -> m Move) -> Strategy m
makeStatelessStrategy f = S
  { strategyState  = ()
  , strategyUpdate = trivialUpdate
  , strategyAction = lift . f
  }
