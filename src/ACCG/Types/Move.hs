module ACCG.Types.Move (Move(..)) where

import ACCG.Types.Hint (Hint)
import ACCG.Types.PlayerIndex (PlayerIndex)

data Move =
  Play Int | Discard Int | Tell PlayerIndex Hint deriving (Eq, Read, Show)
