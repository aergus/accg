{-# LANGUAGE TemplateHaskell #-}

module ACCG.Types.Card (Card(..), number, colorSet) where

import ACCG.Types.Color (Color)

import Control.Lens (makeLenses)

-- IDEA: actually use Data.Set for colorSet?
data Card = Crd {_number :: Int, _colorSet :: [Color]} deriving (Eq, Ord, Show)

makeLenses ''Card
