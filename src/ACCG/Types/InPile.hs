module ACCG.Types.InPile (InPile(..)) where

data InPile a = Discarded a | OnBoard a deriving (Eq, Show)
