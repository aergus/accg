{-# LANGUAGE TemplateHaskell #-}

module ACCG.Types.PlayerKnowledge ( PlayerKnowledge(..)
                                  , handKnowledge
                                  , knowledgeOf
                                  , myIndex
                                  , others
                                  , public
                                  ) where

import ACCG.Types.GameState (GameState, players, publicKnowledge)
import ACCG.Types.Hint (Hint)
import ACCG.Types.Player (Player, hand)
import ACCG.Types.PlayerIndex (PlayerIndex)
import ACCG.Types.PublicKnowledge (PublicKnowledge)

import Control.Lens ((^.), makeLenses)

import qualified Data.Map.Lazy as Map (Map, (!), delete)

data PlayerKnowledge = PlyK
 { _myIndex       :: PlayerIndex
 , _others        :: Map.Map PlayerIndex Player
 , _handKnowledge :: [[Hint]]
 , _public        :: PublicKnowledge
 } deriving (Eq, Show)

makeLenses ''PlayerKnowledge

{- |
  Extracts the 'ACCG.Types.PlayerKnowledge.PlayerKnowledge' for a given
  'ACCG.Types.PlayerID.PlayerID' from the 'ACCG.Types.GameState.GameState'.
-}
knowledgeOf :: PlayerIndex -> GameState -> PlayerKnowledge
knowledgeOf i s = PlyK
  { _myIndex       = i
  , _others        = Map.delete i (s ^. players)
  , _handKnowledge = map snd $ ((s ^. players) Map.! i) ^. hand
  , _public        =  s ^. publicKnowledge
  }
