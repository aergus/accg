module ACCG.Types.Hint (Hint(..)) where

import ACCG.Types.Color (Color)

data Hint = ColorHint Color | NumberHint Int deriving (Eq, Read, Show)
