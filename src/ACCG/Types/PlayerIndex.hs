module ACCG.Types.PlayerIndex (PlayerIndex) where

import ACCG.Types.CounterVal (CounterVal)

type PlayerIndex = CounterVal
