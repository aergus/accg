module ACCG.Types.Board (Board) where

import ACCG.Types.Color (Color)

import qualified Data.Map.Lazy as Map (Map)

type Board = Map.Map [Color] Int
