{-# LANGUAGE TemplateHaskell #-}

module ACCG.Types.GameParameters ( GameParameters(..)
                                 , cardsFor
                                 , cardsPerColorSet
                                 , cardsPerPlayer
                                 , colorSets
                                 , copiesPerNum
                                 , extraHintFor
                                 , maxHints
                                 , maxMistakes
                                 , numCards
                                 , numPlayers
                                 ) where

import ACCG.Types.Card (Card(..))
import ACCG.Types.Color (Color)

import Control.Lens ((^.), makeLenses)
import Control.Monad (join)

data GameParameters = GP
  { _maxHints         :: Int
  , _maxMistakes      :: Int
  , _numPlayers       :: Int
  , _numCards         :: Int
  , _colorSets        :: [[Color]]
  , _cardsPerPlayer   :: Int
  , _cardsPerColorSet :: Int
  , _copiesPerNum     :: [Int]
  , _extraHintFor     :: [Card]
  } deriving (Eq, Show)

makeLenses ''GameParameters

{- |
  Produces a list of all cards in the game for given
  'ACCG.Types.GameParameters.GameParameters'.

  > cardsFor p = join
  >   [ replicate ((p ^. copiesPerNum) !! (k - 1))
  >               Crd {_colorSet = clrS, _number = k}
  >   | clrS <- p ^. colorSets, k <- [1 .. (p ^. cardsPerColorSet)]
  >   ]
-}
cardsFor :: GameParameters -> [Card]
cardsFor p = join
  [ replicate ((p ^. copiesPerNum) !! (k - 1))
              Crd {_colorSet = clrS, _number = k}
  | clrS <- p ^. colorSets, k <- [1 .. (p ^. cardsPerColorSet)]
  ]
