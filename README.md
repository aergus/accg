# A Cooperative Card Game

ACCG is a Haskell implementation of the cooperative card game
[Hanabi][hanabi-on-wikipedia].

Currently it is just a (working) prototype. The structure of the library is
subject to change.


[hanabi-on-wikipedia]: https://en.wikipedia.org/wiki/Hanabi_%28card_game%29
